package com.deepfriedtech.tagworld1;

//Default imports:
import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.media.MediaPlayer;
import android.media.MediaRecorder;
import android.os.AsyncTask;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;


//My imports:
import android.util.Log;
import android.widget.Toast;


import com.paramsen.noise.Noise;
import com.paramsen.noise.NoiseOptimized;


import java.io.File;

import be.tarsos.dsp.AudioDispatcher;
import be.tarsos.dsp.AudioEvent;
import be.tarsos.dsp.AudioProcessor;
import be.tarsos.dsp.io.android.AudioDispatcherFactory;
import be.tarsos.dsp.pitch.PitchDetectionHandler;
import be.tarsos.dsp.pitch.PitchDetectionResult;
import be.tarsos.dsp.pitch.PitchProcessor;

public class MainActivity extends AppCompatActivity
{

    final int SAMPLE_RATE = 44100;
    final int BUFFER_SIZE = 2048;

    //Audio detection parameters:
    final int TARGET_PITCH = 15192;
    //Seems to be the highest I can get it to detect right now
    //The detection actually seems to be off by 200hz or so compared to tone generators...
    //This actually represents 15000Hz
    final int PITCH_VARIANCE =  10; //The number of Hz out from TARGET_PITCH it can be to still be a hit.

    boolean detected = false;

    //Permisison trackers:
    public static final int AUDIO_REC_TRACKER = 1;

    MediaPlayer tagSoundPlayer;

    // Used to load the 'native-lib' library on application startup.
    static
    {
        System.loadLibrary("native-lib");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        Log.d("tagLogs","onCreate" );

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Example of a call to a native method
        TextView tv = (TextView) findViewById(R.id.sample_text);
        tv.setText(stringFromJNI());

        //Setting up the button:
        final android.widget.Button tagButton = findViewById(R.id.tagButton);

        tagButton.setOnClickListener(tagButtonListener);
        tagButton.setOnClickListener(tagButtonListener);

        //Setting up the audio
        tagSoundPlayer = MediaPlayer.create(this, R.raw.beep);

        checkPermissions();


    }

    /**
     * A native method that is implemented by the 'native-lib' native library,
     * which is packaged with this application.
     */
    public native String stringFromJNI();

    private void checkPermissions()
    {
        Log.d("tagLogs","checkPermissions()");

        //Permission stuff:
        if(ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.RECORD_AUDIO)
                == PackageManager.PERMISSION_GRANTED){
            Log.d("tagLogs", "Permission to record audio: yes :)");
            try {
                setupAudio();
            }
            catch (Exception e){
                Log.d("tagLogs", "Exception: " + e.toString());

            }
        }
        else
        {
            Log.d("tagLogs", "Permission to record audio: no. Will request if possible.");

            //No permission to record audio, must request it
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.RECORD_AUDIO}, AUDIO_REC_TRACKER);

        }
    }


    //@Override
    public void onRequestPermissionsResult(int requestCode,
                                          String permissions[], int[] grantResults){

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        Log.d("tagLogs", "onRequestPermissionsResult called");

        switch (requestCode){
            case AUDIO_REC_TRACKER:{
                Log.d("tagLogs", "Permission for audio recording gained.");

                try {
                    setupAudio();
                }
                catch (Exception e){
                    Log.d("tagLogs", "Exception: " + e.toString());

                }


                //Todo: The audio fails to work the first time the permission is granted, despite the call above.
                //For now, you have to exit the app and start it again AFTER you granted audio recording permision.

                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
                return;
            }
        }
    }


    private void setupAudio() {


        Log.d("tagLogs", "setupAudio called");

        PitchDetectionHandler tagDetectionHandler = new PitchDetectionHandler()
        {
            @Override
            public void handlePitch(PitchDetectionResult pitchDetectionResult, AudioEvent audioEvent)
            {
                final float pitchHz = pitchDetectionResult.getPitch();


                /*
                Runnable tRunnable = new Runnable() {
                    @Override
                    public void run() {
                        processPitch(pitchHz);
                    }
                };

                runOnUiThread(tRunnable);
                */



                class SoundTask extends AsyncTask<Void, Void, Void>{
                    @Override
                    protected Void doInBackground(Void... params ){
                        processPitch(pitchHz);

                        return null;
                    }


                    @Override
                    protected void onPostExecute(Void aVoid){

                        if(detected){
                            soundDetected();
                        }

                        super.onPostExecute(aVoid);
                    }
                }

                new SoundTask().execute();



            }
        };

        //Sample rate of 44100, buffer size of 1024, buffer overlap of 0
        AudioDispatcher tagDispatcher = AudioDispatcherFactory.fromDefaultMicrophone(
                SAMPLE_RATE, BUFFER_SIZE, 0);

        Log.d("tagLogs", "tagDispatcher.toString()" + tagDispatcher.toString());

        AudioProcessor tagPitchProcessor = new PitchProcessor(PitchProcessor.PitchEstimationAlgorithm.FFT_YIN,
                SAMPLE_RATE, BUFFER_SIZE, tagDetectionHandler);
        tagDispatcher.addAudioProcessor(tagPitchProcessor);

        new Thread(tagDispatcher, "tagDispatcher").start();
    }

    private void processPitch(float pitch)
    {
        Log.d("tagLogs","Pitch: " + pitch);

        if(pitch < (TARGET_PITCH + PITCH_VARIANCE) && pitch > (TARGET_PITCH - PITCH_VARIANCE))
        {
            detected = true;
        }
    }

    private void soundDetected()
    {
        Log.d("tagLogs", "Target pitch detected!");

        //Toast:
        Toast.makeText(getApplicationContext(),"You're it!", Toast.LENGTH_SHORT).show();
    }


    private View.OnClickListener tagButtonListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            Log.d("tagLogs", "Tag buttton pressed");


            tagSoundPlayer.start(); //Plays the sound
        }
    };

    private View.OnClickListener listenButtonListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            Log.d("tagLogs", "Listen buttton pressed");
        }
    };


    private void startAudio()
    {
        //Audio code:
        MediaRecorder tagRecorder = new android.media.MediaRecorder();
        tagRecorder.setAudioSource(MediaRecorder.AudioSource.UNPROCESSED);
        tagRecorder.setOutputFormat(MediaRecorder.OutputFormat.WEBM);

        //File audioFile = new File();
        //tagRecorder.setOutputFile();

    }
}
