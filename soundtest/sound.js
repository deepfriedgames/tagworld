var recordLink, stopLink, mRecorder;

const checksPerSecond = 1; //How often to save/analyze audio

const timeSlice = 1000/checksPerSecond; //Time in ms after which to analyze audio

var audioChunks = [];

//Analysis stuff:
var audioContext;
var audioAnalyser;

var bufferLen;
var audioDataArray;


//Canvas stuff:
var audioCanvas;
var audioCanvasContext;

//--
var animTimeOut;

window.onload = function()
{
    //Canvas stuff:
    audioCanvas = document.getElementById("audioCanv");
    audioCanvasContext = audioCanvas.getContext("2d");

    
    //Audio and audio analysis stuff:
    audioContext = new window.webkitAudioContext();
    audioAnalyser = audioContext.createAnalyser();
    
    
    audioAnalyser.fftSize = 512;
    bufferLen = audioAnalyser.frequencyBinCount;
    
    audioDataArray = new Uint8Array(bufferLen);
    audioAnalyser.getByteTimeDomainData(audioDataArray);
    
    
    console.log("fftSize: " + audioAnalyser.fftSize);
    
    navigator.mediaDevices.getUserMedia( {audio: true})
        .then(function(stream) { 
    
        mRecorder = new MediaRecorder(stream);
        
        document.getElementById('startLink').addEventListener('click', start);
        document.getElementById('stopLink').addEventListener('click', stop);
        document.getElementById('playLink').addEventListener('click', play);
        
        mRecorder.addEventListener('dataavailable', onRecordingData);
        });
    
    
};

function drawCanvas()
{
    audioCanvasContext.fillStyle = "rgb(150, 150, 300)";
    audioCanvasContext.fillRect(0, 0, audioCanvas.width, audioCanvas.height);

    
    //requestAnimationFrame(drawCanvas);
    
    animTimeOut = setTimeout(drawCanvas, timeSlice);
}

function start()
{
    console.log("Start button clicked");
    mRecorder.start(timeSlice);
    
    //requestAnimationFrame(drawCanvas)

    setTimeout(drawCanvas, timeSlice);
}


function onRecordingData(e)
{
    console.log("Pushing chunks");
    audioChunks.push(e.data);
    
    
        
    audioAnalyser.getByteTimeDomainData(audioDataArray);
    
    console.log("bufferLen: " + bufferLen);
    
    console.log("audioAnalyser.fftSize: " + audioAnalyser.fftSize)
    
    console.log("audioAnalyser.frequencyBitCount: " + audioAnalyser.frequencyBitCount)
    
    console.log("audioDataArray[0]: " + audioDataArray[0]);
    
}

function stop()
{
    console.log("Stopping recording");
    mRecorder.stop();
    
    clearTimeout(animTimeOut);
}

function play()
{
    console.log("Attempting to play...");
    
    var audioBlob = new Blob(audioChunks, {type: 'audio/ogg'});
    var audioElement = document.getElementById('audioPlayer');
    
    var blobUrl = URL.createObjectURL(audioBlob);
    
    var sourceElement = document.createElement('source');
    
    sourceElement.src = blobUrl;
    sourceElement.type = 'audio/wav';
    audioElement.appendChild(sourceElement);
    
    audioElement.play();
}