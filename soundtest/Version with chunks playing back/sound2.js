var recordLink, stopLink, mRecorder;
const timeSlice = 300; //Time in ms after which to analyze audio

var audioChunks = [];

window.onload = function()
{
    navigator.mediaDevices.getUserMedia( {audio: true})
        .then(function(stream) { 
    
        mRecorder = new MediaRecorder(stream);
        
        document.getElementById('startLink').addEventListener('click', start);
        document.getElementById('stopLink').addEventListener('click', stop);
        document.getElementById('playLink').addEventListener('click', play);
        
        mRecorder.addEventListener('dataavailable', onRecordingData);
        });
};

function start()
{
    console.log("Start button clicked");
    mRecorder.start(timeSlice);
}


function onRecordingData(e)
{
    console.log("Pushing chunks");
    audioChunks.push(e.data);
}

function stop()
{
    console.log("Stopping recording");
    mRecorder.stop();
}

function play()
{
    console.log("Attempting to play...");
    
    var audioBlob = new Blob(audioChunks, {type: 'audio/ogg'});
    var audioElement = document.getElementById('audioPlayer');
    
    var blobUrl = URL.createObjectURL(audioBlob);
    
    var sourceElement = document.createElement('source');
    
    sourceElement.src = blobUrl;
    sourceElement.type = 'audio/wav';
    audioElement.appendChild(sourceElement);
    
    audioElement.play();
    
    /*
    console.log("Attempting to play...");
    
    var audioElement = document.getElementById('audioPlayer');
    audioElement.src = URL.createObjectURL(e.data);
    audioElement.play();
    */
}